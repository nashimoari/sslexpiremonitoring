# SSLExpireMonitoring

SSL Expire monitoring<br>
Based on this topic: https://serverlesscode.com/post/ssl-expiration-alerts-with-lambda/

### domains.txt
The file domains.txt contains a list of domains to be scanned.<br>
Each domain is on a new line.<br>
For example:<br>
test.abc <br>
wildcard.test.abc 
