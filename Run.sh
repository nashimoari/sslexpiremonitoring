#!/bin/sh

# удаляем существующий контейнер по имени (на всякий случай)
docker rm SSLExpireCheck

# Запускаем проверку и при завершении удаляем контейнер.
docker run --rm  \
-v /var/log/SSLExpireChecker:/usr/src/app/logs \
-v "${PWD}"/domains.txt:/usr/src/app/domains.txt:ro \
--name SSLExpireCheck ssl-expire-check:latest
